
public class Notendurchschnitt {

	public static double schnitt (double [] noten) {
		
		double summe = 0;
		int anzahl = noten.length;
		
		for (int i=0; i<noten.length; i++) {
			summe= summe + noten [i];
			
		}
		
		double schnitt = summe / anzahl;
		return schnitt;
	}
	
	public static void main (String [] args) {
		double [] meineN = {3,5,4,1,3,3};
		System.out.println("Mein Durchschnitt ist:" + schnitt(meineN));
	}
}
